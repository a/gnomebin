import sanic
import os
import json
import config
import secrets
import random

app = sanic.Sanic()

pastes = {}


@app.route("/")
async def main(request):
    return sanic.response.text(config.main_text)


@app.post("/add")
async def add_paste(request):
    global pastes
    if "text" not in request.form or not request.form["text"]:
        return sanic.response.json({"success": False,
                                    "reason": "No content given"})
    paste_id = secrets.token_urlsafe(6)
    while paste_id in pastes:
        paste_id = secrets.token_urlsafe(6)
    pastes[paste_id] = request.form["text"][0]
    save_state()
    return sanic.response.json({"success": True, "id": paste_id})


@app.get("/paste/<paste_id>")
async def get_paste(request, paste_id):
    global pastes
    if paste_id not in pastes:
        return sanic.response.text("No such paste.")

    cookie_name = f"canview-{paste_id}"
    if cookie_name in request.cookies and request.cookies[cookie_name]:
        return sanic.response.text(pastes[paste_id])
    else:
        # TODO: Get actual security
        ad = random.choice(config.ads) + config.ad_disclaimer
        resp = sanic.response.text(ad,
                                   headers={"Refresh": str(config.ad_duration)})
        resp.cookies[cookie_name] = "True"
        return resp


def save_state():
    global pastes
    with open(config.state_file, "w") as f:
        json.dump(pastes, f)


def load_state():
    global pastes
    if not os.path.exists(config.state_file):
        with open(config.state_file, "w") as f:
            f.write("{}")
        return

    with open(config.state_file, "r") as f:
        pastes = json.load(f)


if __name__ == '__main__':
    load_state()
    app.run(host=config.sanic_host, port=config.sanic_port)
